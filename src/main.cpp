#include <dpp/dpp.h>
#include <cstdlib>
#include <string>
#include <iostream>


std::string testCode = "TheCode";//replace this with actual way of accessing codes.

int main() {
//TOKEN
    dpp::cluster bot("TOKEN");//TOKEN FOR BOT FROM DISCORD DEVELOPER PORTAL.
        bot.on_log(dpp::utility::cout_logger());

    bot.on_slashcommand([&bot](const dpp::slashcommand_t & event) {
        if (event.command.get_command_name() == "verify") {
            std::string codeIn = std::get<std::string>(event.get_parameter("code"));

	    if(testCode==codeIn)//checks code provided by user and if it matches it will add role
        {
	    //code for role assignment

             dpp::role thistrole;
             thistrole.permissions = dpp::p_administrator;
             thistrole.guild_id = 1044859463494008862;//ID of a server bot will be on.get it by right clicking server above channels and clicking last options
             thistrole.set_name("verified user");
             bot.role_create(thistrole, [&bot, event](auto& e) {
                 if (!e.is_error()) {
                     dpp::role r = std::get<dpp::role>(e.value);
                     bot.guild_member_add_role(event.command.guild_id, event.command.usr.id, r.id);
                 }

                 }
                 );


	    }
	    else{
		event.reply(std::string("please double check the code you have provided...\nMuch obliged\nServer staff"));
	    }
        }
    });

    bot.on_ready([&bot](const dpp::ready_t & event) {
        if (dpp::run_once<struct register_bot_commands>()) {

            dpp::slashcommand newcommand("verify", "Verifies code for further server access", bot.me.id);
	    newcommand.add_option(dpp::command_option(dpp::co_string,"code","Code for server access",true ));

            bot.global_command_create(newcommand);
        }
    });

    bot.start(dpp::st_wait);

    return 0;
}
